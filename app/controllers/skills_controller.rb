class SkillsController < ApplicationController
   before_action :logged_in_user
   before_action :admin_user
  def new
    @skill = Skill.new
  end

  def create
    @skill = Skill.new(skill_params)
    if @skill.save
      # Handle a successful save.
      flash[:success] = "Skill successfully added"
      redirect_to skills_path
    else
      render 'new'
    end
  end

  def update
  end

  def index
    @skills = Skill.all
  end

  def show
  end

  def destroy
  end
end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def skill_params
      params.require(:skill).permit(:name, :link, :image_name, :category, :sub_category)
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end