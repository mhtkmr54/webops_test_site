class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def contact
  end

  def resource
    @skills = Skill.all
  end
end
