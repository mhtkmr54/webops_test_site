class Skill < ActiveRecord::Base
    before_save :downcase_all
    private
        # Converts roll to all lower-case.
        def downcase_all
          self.name.downcase!
          self.link.downcase!
          self.image_name.downcase!
          self.category.downcase!
          self.sub_category.downcase!
        end
end
