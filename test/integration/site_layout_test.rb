require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:Abhishek)
  end

  test "home layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", resource_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", about_path
  end

  test "should show the roll number in header" do
    log_in_as @user
    follow_redirect!
    assert_select "a[class='dropdown-toggle']", "#{@user.roll.upcase}", count:1
  end
end
