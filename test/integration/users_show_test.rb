require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:Abhishek)
    @other_user = users(:Bhupinder)
  end

  test "should show name if non-empty" do
    log_in_as @user
    follow_redirect!
    assert_template "users/show"
    assert_select "h1", "#{@user.name.capitalize} | #{@user.roll.upcase}"
  end

  test "should not show name if empty" do
    log_in_as @other_user
    follow_redirect!
    assert_template "users/show"
    assert_select "h1", "#{@other_user.roll.upcase}"
  end
end
