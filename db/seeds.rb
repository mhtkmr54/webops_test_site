# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "Abhishek",
             roll: "mm12b037",
             admin: true,
             activated: true,
             password:              "foobar",
             password_confirmation: "foobar")
User.create!(name:  "Arun",
             roll: "ch14b009",
             admin: true,
             activated: true,
             password:              "foobar",
             password_confirmation: "foobar")
User.create!(name:  "Mohit",
             roll: "mm13b025",
             admin: true,
             activated: true,
             password:              "foobar",
             password_confirmation: "foobar")
Skill.create!(  name: "html5",
                link: "http://www.w3.org/html/",
                image_name: "html5.gif",
                category: "webops",
                sub_category: "frontend")
Skill.create!(  name: "css3",
                link: "http://www.w3.org/Style/CSS/",
                image_name: "css.jpg",
                category: "webops",
                sub_category: "frontend")
Skill.create!(  name: "django",
                link: "https://www.djangoproject.com/",
                image_name: "dj.jpg",
                category: "webops",
                sub_category: "backend")
Skill.create!(  name: "rails",
                link: "http://rubyonrails.org/",
                image_name: "rails.jpg",
                category: "webops",
                sub_category: "backend")